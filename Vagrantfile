# -*- mode: ruby -*-
# vi: set ft=ruby :

APP_NAME = ENV["APP_NAME"]
APP_NAME ||= File.basename(Dir.getwd)

HOST_PORT = ENV["HOST_PORT"].to_i
HOST_PORT = 8080 if HOST_PORT == 0

NEXTCLOUD_VERSION = 'latest'

ADMIN_USER = ENV["ADMIN_USER"]
ADMIN_USER ||= "admin"

ADMIN_PASS = ENV["ADMIN_PASS"]
ADMIN_PASS ||= "itequig"

# TODO: make sure the version actually exists
def zipfile_from_version(version)
  return "nextcloud-#{version}.zip" if version

  return "latest.zip"
end

ZIPFILE = zipfile_from_version(ENV["NEXTCLOUD_VERSION"])

APACHE_CONFIG=<<END_OF_APACHE_CONFIG
<VirtualHost *:80>
  DocumentRoot /var/www/nextcloud
  ServerName  localhost

  <Directory /var/www/nextcloud>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
END_OF_APACHE_CONFIG

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"

  config.vm.network "forwarded_port", guest: 80, host: HOST_PORT
  config.vm.synced_folder ".", "/var/www/nextcloud/apps/#{APP_NAME}", :owner => "www-data", :create => true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get -qq install -y apache2

    # install required PHP modules
    apt-get -qq install -y php php-curl php-fdomdocument php-gd php-mbstring php-zip php-xml php-zip

    # install the recommended PHP modules
    apt-get -qq install -y php-bz2 php-intl

    # install the PHP redis module, this is required in the default config
    apt-get -qq install -y php-redis

    # install the packages and PHP modules for pictures
    apt-get -qq install -y php-exif php-imagick

    # add SQLite and the PHP connector
    apt-get -qq install -y sqlite3 php-sqlite3

    # make sure we can get and unpack nextcloud
    apt-get -qq install -y wget unzip

    # download and extract nextcloud
    wget --quiet -O /tmp/nextcloud.zip https://download.nextcloud.com/server/releases/#{ZIPFILE}
    unzip -qq /tmp/nextcloud.zip -d /var/www
    chown -R www-data.www-data /var/www/nextcloud


    # configure apache for nextcloud
    a2enmod rewrite
    a2enmod headers
    sudo echo "#{APACHE_CONFIG}" > /etc/apache2/sites-available/nextcloud.conf
    a2ensite nextcloud
    service apache2 restart

    # set up Nextcloud and enable the app
    sudo -u www-data php /var/www/nextcloud/occ maintenance:install --admin-user #{ADMIN_USER} --admin-pass #{ADMIN_PASS}
    sudo -u www-data php /var/www/nextcloud/occ app:enable #{APP_NAME}

  SHELL
end

