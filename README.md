# Using Vagrant with Nextcloud

You can use Vagrant to develop Nextcloud applications.  To use it,
copy `Vagrantfile` in to the root of your application and run:

    $ vagrant up

This will create a vagrant VM with the latest nextcloud, install your
app and enable it.


## Configuration

Configuration is provided via environment variables.  They are:

| variable | default | description |
| -------- | ------- | ----------- |
| APP_NAME | Current directory name. | This is the name of your application, and is used for the directory under `apps/` where it will be installed in Nextcloud.  The default is the name of the current directory. |
| HOST_PORT | 8080 | The port on the host system that is forwarded to port 80 in the VM |
| NEXTCLOUD_VERSION | `latest` | The version of Nextcloud to install in the VM. This can be useful for testing your application against different versions of Nextcloud. |
| ADMIN_USER | `admin` | The username of the admin user that is created. |
| ADMIN_PASS | `itequig` | The password for the created admin user. |


# Rsync

You have to run "vagrant rsync" every time you want your changes to be
recognized by the vagrant VM.

    $ vagrant rsync

It is possible that this is only required on macOS hosts, but this is not tested thoroughly.

# Rebuild

Sometimes you want to rebuild everything from scratch, easily do this
like so:

    $ vagrant destroy -f && vagrant up
